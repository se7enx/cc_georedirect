<?php /* #?ini charset="utf-8"?

[General]
# The list of siteaccess for which georedirect is skipped
ExcludeSiteaccesses[]
ExcludeSiteaccesses[]=site_admin
# The list of HTTP merhods for which georedirect is skipped
ExcludeHTTPMethods[]
ExcludeHTTPMethods[]=POST
# The list of paths for which georedirect is skipped. You can add custom modules here
ExcludePaths[]
ExcludePaths[]=/custom_module/view
$ The list of get parameters with their values for which geo redirect should be disabled
ExcludeGETParams[]
ExcludeGETParams[param1]=value1
# The list of georedirect rules. They need to be overrided
Rules[]
#Rules[]=rule1

# Example rule1
[Rule_rule1]
# The list of siteaccess for which this rule is applied (optional)
SiteAccesses[]
#SiteAccesses[]=site
# The list of exclude country codes (optional). Has top priority over CountryCodes and CountryNames
ExcludeCountryCodes[]
ExcludeCountryCodes[]=US
# The list of country names for which this rule is active
CountryNames[]
CountryNames[]=Ukraine
CountryNames[]=Hungary
# The list of country codes for which this rule is active
CountryCodes[]
CountryCodes[]=SK
CountryCodes[]=US
# The list of cookies with their values which should be set to make this rule active
IncludeCookies[]
IncludeCookies[cookie1]=val1
# The list of cookies with their values which should be not set to make this rule active
ExcludeCookies[]
ExcludeCookies[cookie2]=val2


# Georedirect prefix (optional)
RedirectPrefix=/site1
# Georedirect host (optional)
RedirectHost=geo.site.com
*/ ?>
